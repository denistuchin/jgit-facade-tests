package com.rocketsiebel.jgitfacade;

public class WorkdirObject {
    private String name;
    private boolean isDirectory;

    private int newCount;
    private int modifiedObjects;
    private int removedObjects;


    public WorkdirObject(String name, boolean isDirectory) {
        this.name = name;
        this.isDirectory = isDirectory;
    }


    public String getName() {
        return name;
    }

    public boolean isDirectory() {
        return isDirectory;
    }


    public int getNewCount() {
        return newCount;
    }

    public int getModifiedCount() {
        return modifiedObjects;
    }

    public int getRemovedCount() {
        return removedObjects;
    }


    public boolean isNew() {
        return newCount > 0;
    }

    public boolean isModified() {
        return modifiedObjects > 0;
    }

    public boolean isRemoved() {
        return removedObjects > 0;
    }


    void incNew() {
        this.newCount++;
    }

    void incModified() {
        this.modifiedObjects++;
    }

    void incRemoved() {
        this.removedObjects++;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WorkdirObject)) return false;

        WorkdirObject that = (WorkdirObject) o;
        return !(getName() != null ? !getName().equals(that.getName()) : that.getName() != null);
    }

    @Override
    public int hashCode() {
        return getName() != null ? getName().hashCode() : 0;
    }

    @Override
    public String toString() {
        return "{" +
                "\"name\":\"" + name + "\", " +
                (isDirectory() ? "\"type\":\"dir\"" : "\"type\":\"file\"" ) + ", " +
                (isNew() ? (!isDirectory ? "\"status\":\"new\"" : "" ) : "") +
                (isModified() ? (!isDirectory ? "\"status\":\"modified\"" : "") : "") +
                (isRemoved() ? (!isDirectory ? "\"status\":\"removed\"" : "") : "") +
                (isDirectory() ? "\"new\":\"" + getNewCount() + "\"," : "")  +
                (isDirectory() ? "\"modified\":\"" + getModifiedCount() + "\"," : "" )  +
                (isDirectory() ? "\"removed\":\"" + getRemovedCount() + "\"" : "" )  +
                "}\n";
    }
}
