package com.rocketsiebel.jgitfacade;

import org.eclipse.jgit.api.DiffCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.diff.*;
import org.eclipse.jgit.lib.*;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.AbstractTreeIterator;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathFilter;

import org.gitective.core.CommitFinder;
import org.gitective.core.PathFilterUtils;
import org.gitective.core.filter.commit.CommitListFilter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.diff.JsonDiff;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Nonnull;

/**
 * @author Eugene Krivosheyev
 *
 * Wrapper Facade for JGit API for pleasure development experience with JGit.
 * Warning! Neither subdirs nor branches supported yet.
 *
 */

public class JGitFacade {
    //region LOGGER
    private Logger log = LoggerFactory.getLogger(JGitFacade.class);
    //endregion

    //region FIELDS
    @Nonnull private final File repoDir;
    //endregion

    //region CONSTRUCTORS
    public JGitFacade(@Nonnull String repoDirName) {
        this(new File(repoDirName));
    }

    public JGitFacade(@Nonnull File repoDir) {
        this.repoDir = repoDir;
    }
    //endregion

    /**
     * @return Collection of filenames at working dir in states: Added, Changed, Modified, Untracked.
     */
    public Iterable<String> getNotYetCommitedFileNames() {
        synchronized (JGitFacade.class) {
            try (Git git = Git.open(repoDir)) {

                Status status = git.status().call();

                List<String> fileNames = new LinkedList<>();
                fileNames.addAll(status.getAdded());
                fileNames.addAll(status.getChanged());
                fileNames.addAll(status.getModified());
                fileNames.addAll(status.getUntracked());

                return fileNames;

            } catch (IOException | GitAPIException e) {
                log.error("", e);
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * @param commitAHash Short or long SHA string.
     * @param commitBHash Short or long SHA string.
     * @return Diff object encapsulating diff entries and plain diff text.
     */
    public Diff getDiffForAllFilesAgainstTwoCommits(String commitAHash, String commitBHash) {
        synchronized (JGitFacade.class) {
            try (Git git = Git.open(repoDir)) {

                OutputStream diffText = new ByteArrayOutputStream();
                List<DiffEntry> diffEntries = git.diff().setOutputStream(diffText)
                        .setOldTree(getTreeIterator(git, commitAHash))
                        .setNewTree(getTreeIterator(git, commitBHash))
                        .call();

                return new Diff(diffEntries, diffText.toString());

            } catch (IOException | GitAPIException e) {
                log.error("", e);
                throw new RuntimeException(e);
            }
        }
    }

    public Diff getDiffForAllNotStagedFilesAgainstHEAD() {
        synchronized (JGitFacade.class) {
            try (Git git = Git.open(repoDir)) {

                OutputStream diffText = new ByteArrayOutputStream();
                List<DiffEntry> diffEntries = git.diff().setOutputStream(diffText)
                        .call();

                return new Diff(diffEntries, diffText.toString());

            } catch (IOException | GitAPIException e) {
                log.error("", e);
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * @return File content as string with '\n' characters for new lines.
     * @throws IllegalStateException: 'Did not find expected file'
     */
    public String getFileContentFromCommit(String fileName, String commitHash) {
        synchronized (JGitFacade.class) {
            try (Git git = Git.open(repoDir)) {
                final Repository repo = git.getRepository();
                ByteArrayOutputStream fileContent = new ByteArrayOutputStream();

                ObjectId lastCommitId = repo.resolve(commitHash);
                RevWalk revWalk = new RevWalk(repo);
                RevCommit commit = revWalk.parseCommit(lastCommitId);
                RevTree tree = commit.getTree();

                TreeWalk treeWalk = new TreeWalk(repo);
                treeWalk.addTree(tree);
                treeWalk.setRecursive(true);
                treeWalk.setFilter(PathFilter.create(fileName));
                if (!treeWalk.next()) {
                    throw new IllegalStateException("Did not find expected file " + fileName);
                }

                ObjectId objectId = treeWalk.getObjectId(0);
                ObjectLoader loader = repo.open(objectId);

                loader.copyTo(fileContent);
                revWalk.dispose();

                return fileContent.toString();

            } catch (IOException e) {
                log.error("", e);
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * @return File content as string with '\n' characters for new lines.
     */
    public String getFileContent(String fileName) {
        synchronized (JGitFacade.class) {
            try {
                String fileContent = "";
                for (String line : org.apache.commons.io.FileUtils.readLines(new File(repoDir, fileName))) {
                    fileContent = fileContent + line + "\n";
                }

                return fileContent;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * @return List of commits where particular file was changed.
     */
    public List<RevCommit> getCommitsForFileChanges(String fileName) {
        synchronized (JGitFacade.class) {
            try (Git git = Git.open(repoDir)) {

                CommitFinder finder = new CommitFinder(git.getRepository());
                CommitListFilter javaCommits = new CommitListFilter();
                finder.setFilter(PathFilterUtils.andSuffix(fileName));
                finder.setMatcher(javaCommits);
                finder.find();

                return javaCommits.getCommits();

            } catch (IOException e) {
                log.error("", e);
                throw new RuntimeException(e);
            }
        }
    }

    public void stage(String fileName) {
        synchronized (JGitFacade.class) {
            try (Git git = Git.open(repoDir)) {
                if (!(new File(fileName)).exists()) throw new FileNotFoundException(fileName);

                git.add().addFilepattern(fileName).call();

            } catch (IOException | GitAPIException e) {
                log.error("", e);
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Commit for staged file or files.
     * @param fileName Particular file name or '.' for all directory.
     * @param message Commit message. Please use git commit message convention.
     */
    public void commit(String fileName, String message) {
        synchronized (JGitFacade.class) {
            try (Git git = Git.open(repoDir)) {

                git.commit()
                        .setOnly(fileName)
                        .setMessage(message)
                        .call();

            } catch (IOException | GitAPIException e) {
                log.error("", e);
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Commit for staged and not staged file or files .
     * @param fileName Particular file name or '.' for all directory.
     * @param message Commit message. Please use git commit message convention.
     */
    public void commitAll(String fileName, String message) {
        synchronized (JGitFacade.class) {
            try (Git git = Git.open(repoDir)) {

                git.commit()
                        .setOnly(fileName)
                        .setMessage(message)
                        .setAll(true)
                        .call();

            } catch (IOException | GitAPIException e) {
                log.error("", e);
                throw new RuntimeException(e);
            }
        }
    }

    public void commitByAuthor(String fileName, String message, String authorName, String authorEmail) {
        synchronized (JGitFacade.class) {
            try (Git git = Git.open(repoDir)) {

                git.commit()
                        .setOnly(fileName)
                        .setMessage(message)
                        .setAuthor(authorName, authorEmail)
                        .call();

            } catch (IOException | GitAPIException e) {
                log.error("", e);
                throw new RuntimeException(e);
            }
        }
    }

    public void commitByCommiter(String fileName, String message, String commiterName, String commiterEmail) {
        synchronized (JGitFacade.class) {
            try (Git git = Git.open(repoDir)) {

                git.commit()
                        .setOnly(fileName)
                        .setMessage(message)
                        .setCommitter(commiterName, commiterEmail)
                        .call();

            } catch (IOException | GitAPIException e) {
                log.error("", e);
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Warning: does not work with branches yet.
     * @return List of commits.
     */
    public List<RevCommit> getCommits() {
        synchronized (JGitFacade.class) {
            try (Git git = Git.open(repoDir)) {

                List<RevCommit> commits = new LinkedList<>();
                Iterable<RevCommit> commitsFromLog = git.log().call();
                for (RevCommit commit : commitsFromLog) {
                    commits.add(commit);
                }

                return commits;

            } catch (IOException | GitAPIException e) {
                log.error("", e);
                throw new RuntimeException(e);
            }
        }
    }

    public Diff getDiffForFileAgainstHead(String fileName) {
        synchronized (JGitFacade.class) {
            if (!new File(repoDir, fileName).isFile())
                throw new IllegalArgumentException("Given path is not file.");

            try (Git git = Git.open(repoDir)) {

                OutputStream diffText = new ByteArrayOutputStream();
                List<DiffEntry> diffEntries = git.diff()
                        .setOutputStream(diffText)
                        .setPathFilter(PathFilter.create(fileName))
                        .call();

                return new Diff(diffEntries, diffText.toString());

            } catch (IOException | GitAPIException e) {
                log.error("", e);
                throw new RuntimeException(e);
            }
        }
    }

    public List<WorkdirObject> getChangesetForGivenFolderAgainstHead(String targetFolder) {
        synchronized (JGitFacade.class) {
            if (!new File(repoDir, targetFolder).isDirectory())
                throw new IllegalArgumentException("Given path is not folder.");


            try (Git git = Git.open(repoDir)) {
                DiffCommand diffCommand = git.diff();
                if (!isAssumedAsGitRepoRoot(targetFolder)) {
                    diffCommand.setPathFilter(PathFilter.create(targetFolder));
                }
                List<DiffEntry> diffEntries = diffCommand.call();

                Pattern baseAndNextFolderAgainstElse = Pattern.compile("(" + targetFolder + getEscapedFileSeparator()
                        + "?[^" + getEscapedFileSeparator() + "]*)(.*)");
                Map<String, WorkdirObject> objectsFlags = new HashMap<>();
                for (DiffEntry entry : diffEntries) {
                    String reducedPath;

                    switch (entry.getChangeType()) {
                        case ADD: {
                            Matcher splitter = baseAndNextFolderAgainstElse.matcher(entry.getNewPath());
                            if (splitter.matches()) {
                                reducedPath = splitter.group(1);
                                boolean isDirectory = new File(repoDir, reducedPath).isDirectory();
                                putIfAbsent(objectsFlags, reducedPath, isDirectory).incNew();
                            }
                        }
                        break;

                        case MODIFY: {
                            Matcher splitter = baseAndNextFolderAgainstElse.matcher(entry.getOldPath());
                            if (splitter.matches()) {
                                reducedPath = splitter.group(1);
                                boolean isDirectory = new File(repoDir, reducedPath).isDirectory();
                                putIfAbsent(objectsFlags, reducedPath, isDirectory).incModified();
                            }
                        }
                        break;

                        case DELETE: {
                            Matcher splitter = baseAndNextFolderAgainstElse.matcher(entry.getOldPath());
                            if (splitter.matches()) {
                                reducedPath = splitter.group(1);
                                boolean isDirectory = new File(repoDir, reducedPath).isDirectory();
                                putIfAbsent(objectsFlags, reducedPath, isDirectory).incRemoved();
                            }
                        }
                        break;
                    }
                }

                LinkedList<WorkdirObject> changeset = new LinkedList<>();
                changeset.addAll(objectsFlags.values());
                return changeset;

            } catch (IOException | GitAPIException e) {
                log.error("", e);
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * @return Escaped <code>{@link java.io.File#separator}</code>
     */
    private String getEscapedFileSeparator() {
        if ("\\".equals(File.separator)) {
            return "\\\\";
        }
        return File.separator;
    }

    public List<WorkdirObject> getChangesetForGivenFolderAgainstHeadWithActualState(String targetFolder) {
        synchronized (JGitFacade.class) {
            if (new File(repoDir, targetFolder).listFiles() == null)
                throw new IllegalArgumentException("No target folder exists");


            String fileName;
            List<WorkdirObject> oldset = new LinkedList<>();
            for (File filesystemObject : new File(repoDir, targetFolder).listFiles()) {
                fileName = filesystemObject.getName().replace(File.separator, "");

                if (fileName.startsWith(".")) {
                    continue;
                }
                if (!isAssumedAsGitRepoRoot(targetFolder)) {
                    fileName = targetFolder + File.separator + fileName;
                }

                oldset.add(new WorkdirObject(fileName, filesystemObject.isDirectory()));
            }

            List<WorkdirObject> changeset = getChangesetForGivenFolderAgainstHead(targetFolder);
            for (WorkdirObject oldObject : oldset) {
                if (!changeset.contains(oldObject)) {
                    changeset.add(oldObject);
                }
            }

            return changeset;
        }
    }

    public String getComplexJsonDiffForFileAgainstHead(String relativeFileName) throws IOException {
        synchronized (JGitFacade.class) {
            ObjectMapper mapper = new ObjectMapper();
            mapper.enable(SerializationFeature.INDENT_OUTPUT);

            JsonNode a = mapper.readTree(getFileContentFromCommit(relativeFileName, "HEAD"));
            JsonNode b = mapper.readTree(getFileContent(relativeFileName));
            final JsonNode patch = JsonDiff.asJson(a, b);

            Iterator<JsonNode> elements = patch.iterator();
            while (elements.hasNext()) {
                JsonNode diffNode = elements.next();

                final String path = diffNode.path("path").asText();
                if (path.endsWith("SCRIPT") || path.endsWith("COMMENTS")) {
                    if (diffNode.isObject()) {
                        ObjectNode currentNode = (ObjectNode) diffNode;
                        currentNode.put(
                                "value",
                                diffForStrings(
                                        cd(a, path).asText(),
                                        cd(b, path).asText()
                                )
                        );
                    }
                }

            }

            return patch.toString();
        }
    }


    //region PRIVATE
    private static WorkdirObject putIfAbsent(Map<String, WorkdirObject> objectsFlags, String name, boolean isDirectory) {
        if(!objectsFlags.containsKey(name)) {
            objectsFlags.put(name, new WorkdirObject(name, isDirectory));
        }
        return objectsFlags.get(name);
    }

    private static AbstractTreeIterator getTreeIterator(Git git, String name) throws IOException {
        final ObjectId id = git.getRepository().resolve(name);
        if (id == null) throw new IllegalArgumentException(name);

        final CanonicalTreeParser parser = new CanonicalTreeParser();
        try (ObjectReader reader = git.getRepository().newObjectReader()) {
            parser.reset(reader, new RevWalk(git.getRepository()).parseTree(id));
            return parser;
        }
    }

    private static boolean isAssumedAsGitRepoRoot(String targetFolder) {
        switch (targetFolder) {
            case ".":
            case "./":
            case "":
            case "\\": return true;

            default: return false;
        }
    }

    private static String diffForStrings(String a, String b) throws IOException {
        OutputStream out = new ByteArrayOutputStream();
        RawText stringA = new RawText(a.getBytes());
        RawText stringB = new RawText(b.getBytes());
        EditList diffList = new EditList();
        diffList.addAll(new HistogramDiff().diff(RawTextComparator.DEFAULT, stringA, stringB));
        new DiffFormatter(out).format(diffList, stringA, stringB);
        return out.toString();
    }

    private static JsonNode cd(JsonNode root, String path) {
        if (path.startsWith("/")) {
            path = path.substring(1);
        }

        JsonNode changedDir = root;
        for (String pathElement : path.split("/")) {
            if (pathElement.matches("^\\d+$")) {
                changedDir = changedDir.path(Integer.parseInt(pathElement));
            } else {
                changedDir = changedDir.path(pathElement);
            }
        }
        return changedDir;
    }

    //endregion
}
