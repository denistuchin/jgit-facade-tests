package com.rocketsiebel.jgifacade;

import com.rocketsiebel.jgitfacade.Diff;
import com.rocketsiebel.jgitfacade.JGitFacade;
import com.rocketsiebel.jgitfacade.WorkdirObject;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.util.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.*;
import java.util.List;

import static org.apache.commons.io.FileUtils.*;
import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.*;
import static org.junit.Assume.*;

/**
 * @author Eugene Krivosheyev
 */
public class JGitFacadeTest {
    //region FIELDS
    private static final String REPO_DIR = "gitrepo.test/";
    private File repoDir = new File(REPO_DIR);
    private Git git;
    private JGitFacade jGitFacade = new JGitFacade(repoDir);
    //endregion

    //region FIXTURE
    @Before
    public void setUp() throws GitAPIException, IOException {
        deleteDirectory(repoDir);
        forceMkdir(repoDir);

        git = Git.init().setDirectory(repoDir).call();
        git = Git.open(repoDir);

        jGitFacade = new JGitFacade(repoDir);
    }

    @After
    public void tearDown() throws IOException {
        git.close();
//        deleteDirectory(repoDir);
    }
    //endregion

    @Test
    public void shouldGetNamesForStagedModifiedUntrackedFiles() throws IOException, GitAPIException {
        touch(new File(REPO_DIR, "untrackedFile"));
        touch(new File(REPO_DIR, "stagedFile"));
        touch(new File(REPO_DIR, "changedFile"));
        touch(new File(REPO_DIR, "committedFile"));
        touch(new File(REPO_DIR, "modifiedFile"));

        git.add().addFilepattern("modifiedFile").call();

        git.add().addFilepattern("committedFile").call();
        git.commit().setMessage("").call();

        write(new File(REPO_DIR, "modifiedFile"), "added line", true);

        git.add().addFilepattern("stagedFile").call();

        git.add().addFilepattern("changedFile").call();
        write(new File(REPO_DIR, "changedFile"), "added line", true);
        //endregion

        //region when-then
        assertThat(jGitFacade.getNotYetCommitedFileNames().toString())
                .contains("stagedFile").contains("changedFile").contains("untrackedFile")
                .doesNotContain("committedFile").contains("modifiedFile");
        //endregion
    }

    @Test
    public void shouldGetDiffForAllFilesAgainstTwoDifferentCommits() throws IOException, GitAPIException {
        //region given
        write(new File(REPO_DIR, "fileToModify"), "line 1\nline 2");
        write(new File(REPO_DIR, "fileToAddLine"), "line 1");
        write(new File(REPO_DIR, "fileToRemove"), "line 1");
        git.add()
            .addFilepattern("fileToModify")
            .addFilepattern("fileToAddLine")
            .addFilepattern("fileToRemove")
        .call();
        git.commit().setMessage("first commit").call();

        forceDelete(new File(REPO_DIR, "fileToModify"));
        write(new File(REPO_DIR, "fileToModify"), "modified line 1\nline 2");
        write(new File(REPO_DIR, "fileToAddLine"), "line 2", true);
        forceDelete(new File(REPO_DIR, "fileToRemove"));
        git.add()
            .addFilepattern("fileToModify")
            .addFilepattern("fileToAddLine")
            .addFilepattern("fileToRemove")
        .call();
        git.commit().setAll(true).setMessage("second commit").call();
        //endregion

        //region when
        Diff diff = jGitFacade.getDiffForAllFilesAgainstTwoCommits("HEAD^", "HEAD");
        //endregion

        //region then
        assertEquals(3, diff.getEntries().size());
        assertEquals(DiffEntry.ChangeType.MODIFY, diff.getEntries().get(0).getChangeType());
        assertEquals("fileToAddLine", diff.getEntries().get(0).getOldPath());
        assertEquals("fileToAddLine", diff.getEntries().get(0).getNewPath());

        assertEquals(
                "diff --git a/fileToAddLine b/fileToAddLine\n" +
                        "index dcf168c..3aadaef 100644\n" +
                        "--- a/fileToAddLine\n" +
                        "+++ b/fileToAddLine\n" +
                        "@@ -1 +1 @@\n" +
                        "-line 1\n" +
                        "\\ No newline at end of file\n" +
                        "+line 1line 2\n" +
                        "\\ No newline at end of file\n" +
                        "diff --git a/fileToModify b/fileToModify\n" +
                        "index 0839846..5b03437 100644\n" +
                        "--- a/fileToModify\n" +
                        "+++ b/fileToModify\n" +
                        "@@ -1,2 +1,2 @@\n" +
                        "-line 1\n" +
                        "+modified line 1\n" +
                        " line 2\n" +
                        "\\ No newline at end of file\n" +
                        "diff --git a/fileToRemove b/fileToRemove\n" +
                        "deleted file mode 100644\n" +
                        "index dcf168c..0000000\n" +
                        "--- a/fileToRemove\n" +
                        "+++ /dev/null\n" +
                        "@@ -1 +0,0 @@\n" +
                        "-line 1\n" +
                        "\\ No newline at end of file\n",
                diff.getText().toString());
        //endregion
    }

    @Test
    public void shouldGetNoDiffForAllFilesAgainstTwoSameCommits() throws IOException, GitAPIException {
        //region given
        write(new File(REPO_DIR, "fileToModify"), "line 1\nline 2");
        git.add().addFilepattern("fileToModify").call();
        git.commit().setMessage("").call();
        //endregion

        //region when
        Diff diff = jGitFacade.getDiffForAllFilesAgainstTwoCommits("HEAD", "HEAD");
        //endregion

        //region then
        assertEquals(0, diff.getEntries().size());
        assertEquals("", diff.getText().toString());
        //endregion
    }

    @Test
    public void shouldGetDiffAgainstNonStagedAndHEAD() throws GitAPIException, IOException {
        //region given
        write(new File(REPO_DIR, "fileToModify"), "line 1\nline 2");
        write(new File(REPO_DIR, "fileToAddLine"), "line 1");
        write(new File(REPO_DIR, "fileToRemove"), "line 1");
        git.add()
            .addFilepattern("fileToModify")
            .addFilepattern("fileToAddLine")
            .addFilepattern("fileToRemove")
        .call();
        git.commit().setMessage("").call();

        forceDelete(new File(REPO_DIR, "fileToModify"));
        write(new File(REPO_DIR, "fileToModify"), "modified line 1\nline 2");
        write(new File(REPO_DIR, "fileToAddLine"), "line 2", true);
        forceDelete(new File(REPO_DIR, "fileToRemove"));
        //endregion

        //region when
        Diff diff = jGitFacade.getDiffForAllNotStagedFilesAgainstHEAD();
        //endregion

        //region then
        assertEquals(3, diff.getEntries().size());
        assertEquals(DiffEntry.ChangeType.MODIFY, diff.getEntries().get(0).getChangeType());
        assertEquals("fileToAddLine", diff.getEntries().get(0).getOldPath());
        assertEquals("fileToAddLine", diff.getEntries().get(0).getNewPath());

        assertEquals(
                "diff --git a/fileToAddLine b/fileToAddLine\n" +
                        "index dcf168c..3aadaef 100644\n" +
                        "--- a/fileToAddLine\n" +
                        "+++ b/fileToAddLine\n" +
                        "@@ -1 +1 @@\n" +
                        "-line 1\n" +
                        "\\ No newline at end of file\n" +
                        "+line 1line 2\n" +
                        "\\ No newline at end of file\n" +
                        "diff --git a/fileToModify b/fileToModify\n" +
                        "index 0839846..5b03437 100644\n" +
                        "--- a/fileToModify\n" +
                        "+++ b/fileToModify\n" +
                        "@@ -1,2 +1,2 @@\n" +
                        "-line 1\n" +
                        "+modified line 1\n" +
                        " line 2\n" +
                        "\\ No newline at end of file\n" +
                        "diff --git a/fileToRemove b/fileToRemove\n" +
                        "deleted file mode 100644\n" +
                        "index dcf168c..0000000\n" +
                        "--- a/fileToRemove\n" +
                        "+++ /dev/null\n" +
                        "@@ -1 +0,0 @@\n" +
                        "-line 1\n" +
                        "\\ No newline at end of file\n",
                diff.getText().toString());
        //endregion
    }

    @Test
    public void shouldGetFileContentFromSpecifiedCommit() throws IOException, GitAPIException {
        //region given
        write(new File(REPO_DIR, "fileToModify"), "line 1\nline 2");
        git.add().addFilepattern("fileToModify").call();
        git.commit().setMessage("first commit").call();

        forceDelete(new File(REPO_DIR, "fileToModify"));
        write(new File(REPO_DIR, "fileToModify"), "modified line 1\nline 2");
        git.commit().setAll(true).setMessage("second commit").call();
        //endregion

        //region when-then
        assertThat(jGitFacade.getFileContentFromCommit("fileToModify", "HEAD^").toString())
            .contains("line 1\nline 2")
            .doesNotContain("modified line 1\nline 2");
        //endregion
    }

    @Test(expected = IllegalStateException.class)
    public void shouldThrowExceptionTryingGetUnexistingFileFromSpecifiedCommit() throws IOException, GitAPIException {
        //region given
        write(new File(REPO_DIR, "existingFile"), "line 1\nline 2");
        git.add().addFilepattern("existingFile").call();
        git.commit().setMessage("").call();
        //endregion

        //region when
        try {
            jGitFacade.getFileContentFromCommit("existingFile", "HEAD");
        } catch (IllegalStateException e) {
            assumeNoException(e);
        }
        //endregion

        //region then
        jGitFacade.getFileContentFromCommit("unexistingFile", "HEAD");
        //endregion
    }

    @Test
    public void shouldGetFileContentForAnyFileStateWhenFileExists() throws GitAPIException, IOException {
        //region given
        write(new File(REPO_DIR, "fileToUntrack"), "line 1\nline 2");
        write(new File(REPO_DIR, "fileToStage"), "line 3\nline 4");
        write(new File(REPO_DIR, "fileToCommit"), "line 5\nline 6");

        git.add().addFilepattern("fileToCommit").call();
        git.commit().setMessage("").call();

        git.add().addFilepattern("fileToStage").call();
        //endregion

        //region when
        try {
            jGitFacade.getFileContent(REPO_DIR + "fileNotExists");
        } catch (RuntimeException e) {
            assertThat(e).hasMessage("java.io.FileNotFoundException: File 'gitrepo.test" + File.separator
                    + "fileNotExists' does not exist");
        }
        //endregion

        //region then
        assertThat(jGitFacade.getFileContent(REPO_DIR + "fileToUntrack"))
            .contains("line 1\nline 2");
        assertThat(jGitFacade.getFileContent(REPO_DIR + "fileToStage"))
            .contains("line 3\nline 4");
        assertThat(jGitFacade.getFileContent(REPO_DIR + "fileToCommit"))
            .contains("line 5\nline 6");
        //endregion
    }

    @Test
    public void shouldGetDiffForGivenFileAgainstHead() throws IOException, GitAPIException {
        //region given

        //Initial commit for files and folders not be added but commited
        write(new File(REPO_DIR, "folderToDiff" + File.separator + "dummyFile"), "dummy");
        git.add()
            .addFilepattern("folderToDiff")
            .addFilepattern("folderToDiff" + File.separator + "dummyFile")
        .call();
        git.commit().setMessage("Initial commit").call();

        write(new File(REPO_DIR, "folderToDiff" + File.separator + "fileToModify"), "line 1\nline 2");
        git.add()
            .addFilepattern("folderToDiff" + File.separator + "fileToModify")
        .call();
        git.commit().setMessage("Changes in folderToDiff").call();

        forceDelete(new File(REPO_DIR, "folderToDiff" + File.separator + "fileToModify"));
        write(new File(REPO_DIR, "folderToDiff" + File.separator + "fileToModify"), "modified line 3\nline 4");

        write(new File(REPO_DIR, "folderToDiff" + File.separator + "fileNotToBeInDiff"), "dummy");
        //endregion

        //region when
        Diff diff = jGitFacade.getDiffForFileAgainstHead("folderToDiff" + File.separator + "fileToModify");
        //endregion

        //region then
        assertEquals(1, diff.getEntries().size());

        assertEquals(DiffEntry.ChangeType.MODIFY, diff.getEntries().get(0).getChangeType());
        assertEquals("folderToDiff" + File.separator + "fileToModify", diff.getEntries().get(0).getOldPath());
        assertEquals("folderToDiff" + File.separator + "fileToModify", diff.getEntries().get(0).getNewPath());

        assertEquals(
                "diff --git a/folderToDiff/fileToModify b/folderToDiff/fileToModify\n" +
                        "index 0839846..8e9d15a 100644\n" +
                        "--- a/folderToDiff/fileToModify\n" +
                        "+++ b/folderToDiff/fileToModify\n" +
                        "@@ -1,2 +1,2 @@\n" +
                        "-line 1\n" +
                        "-line 2\n" +
                        "\\ No newline at end of file\n" +
                        "+modified line 3\n" +
                        "+line 4\n" +
                        "\\ No newline at end of file\n",
                diff.getText().toString());
        //endregion
    }

    @Test
    public void shouldGetChangesetForGivenFolderWhenThereAreFilesWithAllStates() throws IOException, GitAPIException {
        //region given

        //region dummy commit
        //Initial commit for files and folders not be added but commited
        write(new File(REPO_DIR, "folderToDiff" + File.separator + "dummyFileToRemove"), "dummy");
        write(new File(REPO_DIR, "folderToDiff" + File.separator + "subfolderToDiff" + File.separator + "subDummyFile"), "subdummy");
        git.add()
                .addFilepattern("folderToDiff")
                .addFilepattern("folderToDiff" + File.separator + "dummyFile")
                .addFilepattern("folderToDiff" + File.separator + "subfolderToDiff")
                .addFilepattern("folderToDiff" + File.separator + "subfolderToDiff" + File.separator + "dummySubFile")
                .call();
        git.commit().setAll(true).setMessage("Initial commit").call();
        //endregion

        write(new File(REPO_DIR, "folderToDiff" + File.separator + "fileNotToStage"), "line 1\nline 2");
        write(new File(REPO_DIR, "folderToDiff" + File.separator + "fileToStageOnly"), "line 1\nline 2");
        write(new File(REPO_DIR, "folderToDiff" + File.separator + "fileToModify"), "line 1\nline 2");
        write(new File(REPO_DIR, "folderToDiff" + File.separator + "subfolderToDiff" + File.separator + "subFileToModify"), "line 3\nline 4");
        write(new File(REPO_DIR, "folderToDiff" + File.separator + "secondSubfolderToDiff" + File.separator + "secondSubFileToModify"), "line 3\nline 4");
        write(new File(REPO_DIR, "folderToDiff" + File.separator + "secondSubfolderToDiff" + File.separator + "secondSubFileToRemove"), "line 3\nline 4");
        git.add()
                .addFilepattern("folderToDiff" + File.separator + "fileToModify")
                .addFilepattern("folderToDiff" + File.separator + "subfolderToDiff" + File.separator + "subFileToModify")
                .addFilepattern("folderToDiff" + File.separator + "secondSubfolderToDiff" + File.separator + "secondSubFileToModify")
                .addFilepattern("folderToDiff" + File.separator + "secondSubfolderToDiff" + File.separator + "secondSubFileToRemove")
                .call();
        git.commit().setMessage("Changes in folderToDiff and subfolderToDiff").call();

        git.add().addFilepattern("folderToDiff" + File.separator + "fileToStageOnly").call();


        forceDelete(new File(REPO_DIR, "folderToDiff" + File.separator + "dummyFileToRemove"));
        write(      new File(REPO_DIR, "folderToDiff" + File.separator + "newFile"), "modified line 5\nline 6");

        forceDelete(new File(REPO_DIR, "folderToDiff" + File.separator + "fileToModify"));
        write(      new File(REPO_DIR, "folderToDiff" + File.separator + "fileToModify"), "modified line 5\nline 6");

        forceDelete(new File(REPO_DIR, "folderToDiff" + File.separator + "subfolderToDiff" + File.separator + "subFileToModify"));
        write(      new File(REPO_DIR, "folderToDiff" + File.separator + "subfolderToDiff" + File.separator + "subFileToModify"), "line 7\nline 8");

        write(      new File(REPO_DIR, "folderToDiff" + File.separator + "subfolderToDiff" + File.separator + "subFileToAdd"), "line 9\nline 10");

        forceDelete(new File(REPO_DIR, "folderToDiff" + File.separator + "secondSubfolderToDiff" + File.separator + "secondSubFileToRemove"));
        forceDelete(new File(REPO_DIR, "folderToDiff" + File.separator + "secondSubfolderToDiff" + File.separator + "secondSubFileToModify"));
        write(      new File(REPO_DIR, "folderToDiff" + File.separator + "secondSubfolderToDiff" + File.separator + "secondSubFileToModify"), "modified line 5\nline 6");

        write(      new File(REPO_DIR, "fileShouldNotBeCountedInDiffBecauseInFilteredFolder"), "nope");
        forceMkdir(new File(REPO_DIR, "folderShouldNotBeCountedInDiffBecauseInFilteredFolder"));
        //endregion

        //region when
        List<WorkdirObject> changeset = jGitFacade.getChangesetForGivenFolderAgainstHead("folderToDiff");
        //endregion

        //region then
        assertEquals(6, changeset.size());

        assertEquals("folderToDiff" + File.separator + "newFile", changeset.get(0).getName());
        assertFalse(changeset.get(0).isDirectory());
        assertTrue(changeset.get(0).isNew());
        assertFalse(changeset.get(0).isModified());
        assertFalse(changeset.get(0).isRemoved());

        assertEquals("folderToDiff" + File.separator + "subfolderToDiff", changeset.get(1).getName());
        assertTrue(changeset.get(1).isDirectory());
        assertEquals(1, changeset.get(1).getNewCount());
        assertEquals(1, changeset.get(1).getModifiedCount());
        assertEquals(0, changeset.get(1).getRemovedCount());

        assertEquals("folderToDiff" + File.separator + "fileToModify", changeset.get(2).getName());
        assertFalse(changeset.get(2).isDirectory());
        assertFalse(changeset.get(2).isNew());
        assertTrue(changeset.get(2).isModified());
        assertFalse(changeset.get(2).isRemoved());

        assertEquals("folderToDiff" + File.separator + "dummyFileToRemove", changeset.get(3).getName());
        assertFalse(changeset.get(3).isDirectory());
        assertFalse(changeset.get(3).isNew());
        assertFalse(changeset.get(3).isModified());
        assertTrue(changeset.get(3).isRemoved());

        assertEquals("folderToDiff" + File.separator + "fileNotToStage", changeset.get(4).getName());
        assertFalse(changeset.get(4).isDirectory());
        assertTrue(changeset.get(4).isNew());
        assertFalse(changeset.get(4).isModified());
        assertFalse(changeset.get(4).isRemoved());

        assertEquals("folderToDiff" + File.separator + "secondSubfolderToDiff", changeset.get(5).getName());
        assertTrue(changeset.get(5).isDirectory());
        assertEquals(0, changeset.get(5).getNewCount());
        assertEquals(1, changeset.get(5).getModifiedCount());
        assertEquals(1, changeset.get(5).getRemovedCount());
        //endregion
    }

    @Test
    public void shouldGetChangesetForGivenFolderAtAnyDepthLevel() throws IOException, GitAPIException {
        //region given

        //region dummy commit
        //Initial commit for files and folders not be added but commited
        write(new File(REPO_DIR, "subfolder1" + File.separator + "subfolder2" + File.separator + "subfolder3" + File.separator + "subfolder4" + File.separator + "subFile4"), "old");
        git.add().addFilepattern("subfolder1" + File.separator + "subfolder2" + File.separator + "subfolder3" + File.separator + "subfolder4" + File.separator + "subFile4").call();
        git.commit().setMessage("Initial commit").call();
        //endregion

        write(new File(REPO_DIR, "subfolder1" + File.separator + "subfolder2" + File.separator + "subfolder3" + File.separator + "subfolder4" + File.separator + "newSubFile4"), "new");
        write(new File(REPO_DIR, "subfolder1" + File.separator + "subfolder2" + File.separator + "subfolder3" + File.separator + "newSubFile3"), "");
        write(new File(REPO_DIR, "subfolder1" + File.separator + "subfolder2" + File.separator + "newSubFile2"), "");
        write(new File(REPO_DIR, "subfolder1" + File.separator + "newSubFile1"), "");
        write(new File(REPO_DIR, "newSubFile0"), "");
        //endregion

        //region when
        List<WorkdirObject> changeset = jGitFacade.getChangesetForGivenFolderAgainstHead("subfolder1" + File.separator + "subfolder2" + File.separator + "subfolder3");
        //endregion

        //region then
        assertEquals(2, changeset.size());

        assertEquals("subfolder1" + File.separator + "subfolder2" + File.separator + "subfolder3" + File.separator + "newSubFile3", changeset.get(0).getName());
        assertFalse(changeset.get(0).isDirectory());
        assertTrue(changeset.get(0).isNew());
        assertFalse(changeset.get(0).isModified());
        assertFalse(changeset.get(0).isRemoved());

        assertEquals("subfolder1" + File.separator + "subfolder2" + File.separator + "subfolder3" + File.separator + "subfolder4", changeset.get(1).getName());
        assertTrue(changeset.get(1).isDirectory());
        assertEquals(1, changeset.get(1).getNewCount());
        assertEquals(0, changeset.get(1).getModifiedCount());
        assertEquals(0, changeset.get(1).getRemovedCount());
        //endregion
    }

    @Test
    public void shouldGetChangesetForRootRepoFolderWhenThereAreFilesAndFolders() throws IOException, GitAPIException {
        //region given

        //region dummy commit
        //Initial commit for files and folders not be added but commited
        write(new File(REPO_DIR, "folderToDiff" + File.separator + "dummyFileToRemove"), "dummy");
        write(new File(REPO_DIR, "folderToDiff" + File.separator + "subfolderToDiff" + File.separator + "subDummyFile"), "subdummy");
        git.add()
                .addFilepattern("folderToDiff")
                .addFilepattern("folderToDiff" + File.separator + "dummyFileToRemove")
                .addFilepattern("folderToDiff" + File.separator + "subfolderToDiff")
                .addFilepattern("folderToDiff" + File.separator + "subfolderToDiff" + File.separator + "dummySubFile")
                .call();
        git.commit().setAll(true).setMessage("Initial commit").call();
        //endregion

        write(new File(REPO_DIR, "folderToDiff" + File.separator + "fileNotToStage"), "line 1\nline 2");
        write(new File(REPO_DIR, "folderToDiff" + File.separator + "fileToStageOnly"), "line 1\nline 2");
        write(new File(REPO_DIR, "folderToDiff" + File.separator + "fileToModify"), "line 1\nline 2");
        write(new File(REPO_DIR, "folderToDiff" + File.separator + "subfolderToDiff" + File.separator + "subFileToModify"), "line 3\nline 4");
        write(new File(REPO_DIR, "folderToDiff" + File.separator + "secondSubfolderToDiff" + File.separator + "secondSubFileToModify"), "line 3\nline 4");
        write(new File(REPO_DIR, "folderToDiff" + File.separator + "secondSubfolderToDiff" + File.separator + "secondSubFileToRemove"), "line 3\nline 4");
        git.add()
                .addFilepattern("folderToDiff" + File.separator + "fileToModify")
                .addFilepattern("folderToDiff" + File.separator + "subfolderToDiff" + File.separator + "subFileToModify")
                .addFilepattern("folderToDiff" + File.separator + "secondSubfolderToDiff" + File.separator + "secondSubFileToModify")
                .addFilepattern("folderToDiff" + File.separator + "secondSubfolderToDiff" + File.separator + "secondSubFileToRemove")
                .call();
        git.commit().setMessage("Changes in folderToDiff and subfolderToDiff").call();

        git.add().addFilepattern("folderToDiff" + File.separator + "fileToStageOnly").call();


        forceDelete(new File(REPO_DIR, "folderToDiff" + File.separator + "dummyFileToRemove"));
        write(new File(REPO_DIR, "folderToDiff" + File.separator + "newFile"), "modified line 5\nline 6");

        forceDelete(new File(REPO_DIR, "folderToDiff" + File.separator + "fileToModify"));
        write(new File(REPO_DIR, "folderToDiff" + File.separator + "fileToModify"), "modified line 5\nline 6");

        forceDelete(new File(REPO_DIR, "folderToDiff" + File.separator + "subfolderToDiff" + File.separator + "subFileToModify"));
        write(new File(REPO_DIR, "folderToDiff" + File.separator + "subfolderToDiff" + File.separator + "subFileToModify"), "line 7\nline 8");

        write(      new File(REPO_DIR, "folderToDiff" + File.separator + "subfolderToDiff" + File.separator + "subFileToAdd"), "line 9\nline 10");

        forceDelete(new File(REPO_DIR, "folderToDiff" + File.separator + "secondSubfolderToDiff" + File.separator + "secondSubFileToRemove"));
        forceDelete(new File(REPO_DIR, "folderToDiff" + File.separator + "secondSubfolderToDiff" + File.separator + "secondSubFileToModify"));
        write(new File(REPO_DIR, "folderToDiff" + File.separator + "secondSubfolderToDiff" + File.separator + "secondSubFileToModify"), "modified line 5\nline 6");

        write(      new File(REPO_DIR, "fileShouldBeCountedInDiffToo"), "nope");
        forceMkdir(new File(REPO_DIR, "folderShouldBeCountedInDiffToo"));
        write(new File(REPO_DIR, "folderShouldBeCountedInDiffToo" + File.separator + "fileShouldBeCountedInDiffToo"), "nope");
        //endregion

        //region when
        List<WorkdirObject> changeset = jGitFacade.getChangesetForGivenFolderAgainstHead(".");
        //endregion

        //region then
        assertEquals(3, changeset.size());

        assertEquals("folderShouldBeCountedInDiffToo", changeset.get(0).getName());
        assertTrue(changeset.get(0).isDirectory());
        assertEquals(1, changeset.get(0).getNewCount());
        assertEquals(0, changeset.get(0).getModifiedCount());
        assertEquals(0, changeset.get(0).getRemovedCount());

        assertEquals("folderToDiff", changeset.get(1).getName());
        assertTrue(changeset.get(1).isDirectory());
        assertEquals(3, changeset.get(1).getNewCount());
        assertEquals(3, changeset.get(1).getModifiedCount());
        assertEquals(2, changeset.get(1).getRemovedCount());

        assertEquals("fileShouldBeCountedInDiffToo", changeset.get(2).getName());
        assertFalse(changeset.get(2).isDirectory());
        assertTrue(changeset.get(2).isNew());
        //endregion
    }

    @Test
    public void shouldGetChangesetForGivenFolderWithDotsInName() throws IOException, GitAPIException {
        //region given
        write(new File(REPO_DIR, "subfolder1" + File.separator + "subfolder2" + File.separator + "subfolder3.extension" + File.separator + "sub-File.extension"), "old");
        git.add().addFilepattern("subfolder1" + File.separator + "subfolder2" + File.separator + "subfolder3.extension" + File.separator + "sub-File.extension").call();
        git.commit().setMessage("Initial commit").call();

        write(new File(REPO_DIR, "subfolder1" + File.separator + "subfolder2" + File.separator + "subfolder3.extension" + File.separator + "sub-File.extension"), "new");
        //endregion

        //region when
        List<WorkdirObject> changesetForFolder = jGitFacade.getChangesetForGivenFolderAgainstHead("subfolder1" + File.separator + "subfolder2");
        List<WorkdirObject> changesetForFile = jGitFacade.getChangesetForGivenFolderAgainstHead("subfolder1" + File.separator + "subfolder2" + File.separator + "subfolder3.extension");
        //endregion

        //region then
        assertEquals(1, changesetForFolder.size());
        assertEquals(1, changesetForFile.size());

        assertEquals("subfolder1" + File.separator + "subfolder2" + File.separator + "subfolder3.extension", changesetForFolder.get(0).getName());
        assertTrue(changesetForFolder.get(0).isDirectory());
        assertEquals(0, changesetForFolder.get(0).getNewCount());
        assertEquals(1, changesetForFolder.get(0).getModifiedCount());
        assertEquals(0, changesetForFolder.get(0).getRemovedCount());

        assertEquals("subfolder1" + File.separator + "subfolder2" + File.separator + "subfolder3.extension" + File.separator + "sub-File.extension", changesetForFile.get(0).getName());
        assertFalse(changesetForFile.get(0).isDirectory());
        assertFalse(changesetForFile.get(0).isNew());
        assertTrue(changesetForFile.get(0).isModified());
        assertFalse(changesetForFile.get(0).isRemoved());
        //endregion
    }

    @Test
    public void shouldGetChangesetWithActualStateForGivenFolder() throws IOException, GitAPIException {
        //region given
        write(new File(REPO_DIR, "subfolder1" + File.separator + "subfolder2" + File.separator + "subfolder3.extension" + File.separator + "changedFile.extension"), "old");
        write(new File(REPO_DIR, "subfolder1" + File.separator + "subfolder2" + File.separator + "subfolder3.extension" + File.separator + "notChangedFile.extension"), "old");
        write(new File(REPO_DIR, "subfolder1" + File.separator + "subfolder2" + File.separator + "subfolder3.extension" + File.separator + "notChangedSubfolder" + File.separator + "dummy"), "old");
        git.add()
            .addFilepattern("subfolder1" + File.separator + "subfolder2" + File.separator + "subfolder3.extension" + File.separator + "changedFile.extension")
            .addFilepattern("subfolder1" + File.separator + "subfolder2" + File.separator + "subfolder3.extension" + File.separator + "notChangedFile.extension")
            .addFilepattern("subfolder1" + File.separator + "subfolder2" + File.separator + "subfolder3.extension" + File.separator + "notChangedSubfolder" + File.separator + "dummy")
        .call();
        git.commit().setMessage("Initial commit").call();

        write(new File(REPO_DIR, "subfolder1" + File.separator + "subfolder2" + File.separator + "subfolder3.extension" + File.separator + "changedFile.extension"), "new");
        //endregion

        //region when
        List<WorkdirObject> changesetForFolder = jGitFacade.getChangesetForGivenFolderAgainstHeadWithActualState("subfolder1" + File.separator + "subfolder2");
        List<WorkdirObject> changesetForFiles = jGitFacade.getChangesetForGivenFolderAgainstHeadWithActualState("subfolder1" + File.separator + "subfolder2" + File.separator + "subfolder3.extension");
        //endregion

        //region then
        assertEquals(1, changesetForFolder.size());
        assertEquals("subfolder1" + File.separator + "subfolder2" + File.separator + "subfolder3.extension", changesetForFolder.get(0).getName());
        assertTrue(changesetForFolder.get(0).isDirectory());
        assertEquals(0, changesetForFolder.get(0).getNewCount());
        assertEquals(1, changesetForFolder.get(0).getModifiedCount());
        assertEquals(0, changesetForFolder.get(0).getRemovedCount());

        assertEquals(3, changesetForFiles.size());
        assertEquals("subfolder1" + File.separator + "subfolder2" + File.separator + "subfolder3.extension" + File.separator + "changedFile.extension", changesetForFiles.get(0).getName());
        assertFalse(changesetForFiles.get(0).isDirectory());
        assertFalse(changesetForFiles.get(0).isNew());
        assertTrue(changesetForFiles.get(0).isModified());
        assertFalse(changesetForFiles.get(0).isRemoved());

        assertEquals("subfolder1" + File.separator + "subfolder2" + File.separator + "subfolder3.extension" + File.separator + "notChangedFile.extension", changesetForFiles.get(1).getName());
        assertFalse(changesetForFiles.get(1).isDirectory());
        assertFalse(changesetForFiles.get(1).isNew());
        assertFalse(changesetForFiles.get(1).isModified());
        assertFalse(changesetForFiles.get(1).isRemoved());

        assertEquals("subfolder1" + File.separator + "subfolder2" + File.separator + "subfolder3.extension" + File.separator + "notChangedSubfolder", changesetForFiles.get(2).getName());
        assertTrue(changesetForFiles.get(2).isDirectory());
        assertEquals(0, changesetForFiles.get(2).getNewCount());
        assertEquals(0, changesetForFiles.get(2).getModifiedCount());
        assertEquals(0, changesetForFiles.get(2).getRemovedCount());
        //endregion
    }

    @Test
    public void shouldGetChangesetWithActualStateForGivenFolderWhenSpacesInNames() throws IOException, GitAPIException {
        //region given
        write(new File(REPO_DIR, "sub folder" + File.separator + "changed File.extension"), "old");
        write(new File(REPO_DIR, "sub folder" + File.separator + "not Changed File.extension"), "old");
        write(new File(REPO_DIR, "sub folder" + File.separator + "not Changed Subfolder" + File.separator + "dummy file"), "old");
        git.add()
                .addFilepattern("sub folder" + File.separator + "changed File.extension")
                .addFilepattern("sub folder" + File.separator + "not Changed File.extension")
                .addFilepattern("sub folder" + File.separator + "not Changed Subfolder" + File.separator + "dummy file")
                .call();
        git.commit().setMessage("Initial commit").call();

        write(new File(REPO_DIR, "sub folder" + File.separator + "not Changed File.extension"), "new");
        //endregion

        //region when
        List<WorkdirObject> changeset = jGitFacade.getChangesetForGivenFolderAgainstHeadWithActualState("sub folder");
        //endregion

        //region then
        assertEquals(3, changeset.size());

        assertEquals("sub folder" + File.separator + "not Changed File.extension", changeset.get(0).getName());
        assertFalse(changeset.get(0).isDirectory());
        assertFalse(changeset.get(0).isNew());
        assertTrue(changeset.get(0).isModified());
        assertFalse(changeset.get(0).isRemoved());

        assertEquals("sub folder" + File.separator + "changed File.extension", changeset.get(1).getName());
        assertFalse(changeset.get(1).isDirectory());
        assertFalse(changeset.get(1).isNew());
        assertFalse(changeset.get(1).isModified());
        assertFalse(changeset.get(1).isRemoved());

        assertEquals("sub folder" + File.separator + "not Changed Subfolder", changeset.get(2).getName());
        assertTrue(changeset.get(2).isDirectory());
        assertEquals(0, changeset.get(2).getNewCount());
        assertEquals(0, changeset.get(2).getModifiedCount());
        assertEquals(0, changeset.get(2).getRemovedCount());
        //endregion
    }

    @Test
    public void shouldNotRepeatFileNamesForRootRepoFolder() throws IOException, GitAPIException {
        //region given
        write(new File(REPO_DIR, "subfolder" + File.separator + "changed File.extension"), "old");
        write(new File(REPO_DIR, "subfolder" + File.separator + "not Changed File.extension"), "old");
        write(new File(REPO_DIR, "subfolder" + File.separator + "not Changed Subfolder" + File.separator + "dummy file"), "old");
        git.add()
                .addFilepattern("subfolder" + File.separator + "changed File.extension")
                .addFilepattern("subfolder" + File.separator + "not Changed File.extension")
                .addFilepattern("subfolder" + File.separator + "not Changed Subfolder" + File.separator + "dummy file")
                .call();
        git.commit().setMessage("Initial commit").call();

        write(new File(REPO_DIR, "subfolder" + File.separator + "not Changed File.extension"), "new");
        //endregion

        //region when
        List<WorkdirObject> changeset = jGitFacade.getChangesetForGivenFolderAgainstHeadWithActualState("");
        //endregion

        //region then
        assertEquals(1, changeset.size());
        //endregion
    }

    @Test
    public void shouldGetComplexJsonDiffForGivenFileAgainstHeadWhenRootFolderAndThereAreChanges() throws IOException, GitAPIException {
        //region given
        write(new File(REPO_DIR, "changed.json"), "{\n" +
                "    \"Test List Applet\": {\n" +
                "        \"BUSINESS_COMPONENT\": \"Test (DataCursors)\",\n" +
                "        \"COMMENTS\": \"LOCKED BY VTAO UI UPGRADE\",\n" +
                "        \"APPLET_LOCALE\": {\n" +
                "            \"ENU-STD\": {\n" +
                "                \"COMMENTS\": \"LOCKED BY VTAO UI UPGRADE\",\n" +
                "                \"APPLICATION_CODE\": \"STD\"\n" +
                "            }\n" +
                "        },\n" +
                "        \"APPLET_SERVER_SCRIPTS\": [\n" +
                "            {\n" +
                "                \"COMMENTS\": \"LOCKED BY VTAO UI UPGRADE\",\n" +
                "                \"NAME\": \"Test\",\n" +
                "                \"PROGRAM_LANGUAGE\": \"JS\",\n" +
                "                \"SCRIPT\": \"function Test(){\\n\\twhile(true)\\n\\t{\\n\\t\\t// Do something\\n\\t\\tbreak;\\n\\t}\\n\\t\\n}\",\n" +
                "                \"SEQUENCE\": \"1\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"COMMENTS\": \"LOCKED BY VTAO UI UPGRADE\",\n" +
                "                \"INACTIVE\": \"N\",\n" +
                "                \"NAME\": \"Test2\",\n" +
                "                \"PROGRAM_LANGUAGE\": \"JS\",\n" +
                "                \"SCRIPT\": \"function Test2()\\n{\\n\\twhile(true)\\n\\t{\\n\\t\\t// Do something\\n\\t\\tbreak;\\n\\t}\\n\\t\\n}\",\n" +
                "                \"SEQUENCE\": \"2\"\n" +
                "            }\n" +
                "        ]\n" +
                "    }\n" +
                "}");
        git.add().addFilepattern("changed.json").call();
        git.commit().setMessage("Initial commit").call();

        write(new File(REPO_DIR, "changed.json"), "{\n" +
                "    \"Test List Applet\": {\n" +
                "        \"BUSINESS_COMPONENT\": \"Test (DataCursors)\",\n" +
                "        \"COMMENTS\": \"LOCKED BY VTAO UI UPGRADE\",\n" +
                "        \"APPLET_LOCALE\": {\n" +
                "            \"ENU-STD\": {\n" +
                "                \"APPLICATION_CODE\": \"NOT STD\",\n" +
                "                \"NEW ATTR\": \"Y\"\n" +
                "            }\n" +
                "        },\n" +
                "        \"APPLET_SERVER_SCRIPTS\": [\n" +
                "            {\n" +
                "                \"COMMENTS\": \"LOCKED BY VTAO UI UPGRADE\",\n" +
                "                \"NAME\": \"Test\",\n" +
                "                \"PROGRAM_LANGUAGE\": \"JS\",\n" +
                "                \"SCRIPT\": \"function Test(){\\n\\twhile(false)\\n\\t{\\n\\t\\t// Do something\\n\\t\\tbreak;\\n\\t}\\n\\t\\n}\",\n" +
                "                \"SEQUENCE\": \"1\"\n" +
                "            }\n" +
                "        ]\n" +
                "    }\n" +
                "}");
        //endregion

        //region when
        String complexJsonDiff = jGitFacade.getComplexJsonDiffForFileAgainstHead("changed.json");
        //endregion

        //region then
        assertEquals(
                "[" +
                    "{" +
                        "\"op\":\"remove\"," +
                        "\"path\":\"/Test List Applet/APPLET_LOCALE/ENU-STD/COMMENTS\"," +
                        "\"value\":\"@@ -1 +0,0 @@\\n-LOCKED BY VTAO UI UPGRADE\\n\\\\ No newline at end of file\\n\"" +
                    "}," +
                    "{" +
                        "\"op\":\"add\"," +
                        "\"path\":\"/Test List Applet/APPLET_LOCALE/ENU-STD/NEW ATTR\"," +
                        "\"value\":\"Y\"" +
                    "}," +
                    "{" +
                        "\"op\":\"replace\"," +
                        "\"path\":\"/Test List Applet/APPLET_LOCALE/ENU-STD/APPLICATION_CODE\"," +
                        "\"value\":\"NOT STD\"" +
                    "}," +
                    "{" +
                        "\"op\":\"remove\"," +
                        "\"path\":\"/Test List Applet/APPLET_SERVER_SCRIPTS/1\"" +
                    "}," +
                    "{" +
                        "\"op\":\"replace\"," +
                        "\"path\":\"/Test List Applet/APPLET_SERVER_SCRIPTS/0/SCRIPT\"," +
                        "\"value\":\"@@ -1,5 +1,5 @@\\n function Test(){\\n-\\twhile(true)\\n+\\twhile(false)\\n \\t{\\n \\t\\t// Do something\\n \\t\\tbreak;\\n\"" +
                    "}" +
                "]", complexJsonDiff);
        //endregion
    }

    /*
    TODO: Fix ignored tests
    1. Use fixture like unignored ones have: manual file preparing w/o using prepared git repo folder
    2. Use apache io FileUtils not own private prepare* methods
    3. Use regions to fold in IDEA with Ctrl + "+" | "-"
    4. Use HEAD or HEAD^ despite of hardcoded SHA codes
    */

    @Ignore @Test
    public void shouldGetCommitListForParticularFileChanges() {
        List<RevCommit> revCommitList = jGitFacade.getCommitsForFileChanges("javaSourceFileToFind.java");

        assertEquals(2, revCommitList.size());
        assertThat(revCommitList.get(0).getName()).startsWith("8b959");
        assertThat(revCommitList.get(0).getShortMessage()).contains("javaSourceFileToFind.java");
        assertThat(revCommitList.get(1).getName()).startsWith("75c86");
        assertThat(revCommitList.get(1).getShortMessage()).contains("javaSourceFileToFind.java");
    }

    @Ignore @Test
    public void shouldDoAddForParticularNewFile() throws IOException, GitAPIException {
        fail();
        // Preparing for test
        String stagedFileName = "stagedFile";
        Status status;

        try(Git git = Git.open(repoDir)) {
            git.rm().addFilepattern(stagedFileName).call();
            prepareTestFile(git, stagedFileName);
            status = git.status().call();
        } //TODO: WTF catch | finally?
        assumeFalse(status.getAdded().contains(stagedFileName));
        assumeTrue(status.getUntracked().contains(stagedFileName));
        // End of preparing for test

        jGitFacade.stage(stagedFileName);

        try(Git git = Git.open(repoDir)) {
            status = git.status().call();
        }
        assertTrue(status.getAdded().contains(stagedFileName));
        assertFalse(status.getUntracked().contains(stagedFileName));
    }

    @Ignore @Test(expected = RuntimeException.class)
    public void shouldThrowExceptionTryingAddForUnexistingFile() throws IOException, GitAPIException {
        fail();
        String unexistingFile = "unexistingFile";
        jGitFacade.stage(unexistingFile);
    }

    @Ignore @Test
    public void shouldDoAddForParticularModifiedFile() throws IOException, GitAPIException {
        fail();
        // Preparing for test
        String stagedModifiesFile = "stagedModifiesFile";
        Status status;

        try(Git git = Git.open(repoDir)) {
            prepareTestFile(git, stagedModifiesFile);
            git.add().addFilepattern(stagedModifiesFile).call();
            modifyFile(git, stagedModifiesFile);
            status = git.status().call();
        }
        assumeTrue(status.getAdded().contains(stagedModifiesFile));
        assumeFalse(status.getUntracked().contains(stagedModifiesFile));
        assumeTrue(status.getModified().contains(stagedModifiesFile));
        // End of preparing for test

        jGitFacade.stage(stagedModifiesFile);

        try(Git git = Git.open(repoDir)) {
            status = git.status().call();
        }
        assertTrue(status.getAdded().contains(stagedModifiesFile));
        assertFalse(status.getUntracked().contains(stagedModifiesFile));
        assertFalse(status.getModified().contains(stagedModifiesFile));
    }

    @Ignore @Test
    public void shouldDoCommitForParticularStagedFile() throws GitAPIException, IOException {
        fail();
        String commitedFileName = "commitedFile";
        Status status;

        try(Git git = Git.open(repoDir)) {
            git.rm().addFilepattern(commitedFileName).call();
            git.commit().setOnly(commitedFileName).setMessage(commitedFileName + " removed").call();
            prepareTestFile(git, commitedFileName);
            git.add().addFilepattern(commitedFileName).call();
            status = git.status().call();
        }
        assumeTrue(status.getAdded().contains(commitedFileName));
        assumeFalse(status.getUntracked().contains(commitedFileName));

        jGitFacade.commit(commitedFileName, commitedFileName + " added");

        try(Git git = Git.open(repoDir)) {
            status = git.status().call();
        }
        assertFalse(status.getAdded().contains(commitedFileName));
        assertFalse(status.getUntracked().contains(commitedFileName));
    }

    @Ignore @Test //TODO: Very unstable test: works for mvn, but does not for IDEA
    public void shouldGetListForCommits() throws IOException, GitAPIException {
        fail();
        List<RevCommit> commits = jGitFacade.getCommits();
        assertTrue(commits.size() > 10);
        //todo: last commits
        assertThat(commits.get(0).getShortMessage()).isEqualTo("Dummy commit");
        assertThat(commits.get(1).getShortMessage()).isEqualTo("javaSourceFileToFind.java mofified");

    }

    //region PRIVATE
    //TODO: refactor tests to apache-commons-io to remove these helpers
    private void prepareTestFile(Git git, String fileName) throws IOException {
        File file = new File(git.getRepository().getWorkTree(), fileName);
        file.delete();
        FileUtils.createNewFile(file);
        PrintWriter writer = new PrintWriter(file);
        writer.print("line 1\nline 2\nline 3");
        writer.close();
    }

    private void modifyFile(Git git, String fileName) throws IOException {
        File file = new File(git.getRepository().getWorkTree(), fileName);
        PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
        writer.print("\nline 4");
        writer.close();
    }
    //endregion
}
