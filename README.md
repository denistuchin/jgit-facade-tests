# Origin
Inspired by Eclipse JGit: [https://github.com/eclipse/jgit](https://github.com/eclipse/jgit)

# Purpose
Wrapper facade for Eclipse JGit project. 
Fix usability issues and make API more coarse-grained and useful.

# Build
For building jar and getting all dependent jars use maven:
```
#!sh

mvn clean verify
```

# Must know #
## How do I use Notepad++ (or any other editor besides vim)git? ##

```
#!cmd

git config --global core.editor "'C:/Program Files/Notepad++/notepad++.exe' -multiInst -notabbar -nosession -noPlugin"
```

Or, for 64-bit Windows and a 32-bit install of Notepad++:


```
#!cmd

git config --global core.editor "'C:/Program Files (x86)/Notepad++/notepad++.exe' -multiInst -notabbar -nosession -noPlugin"
```

Or, the following can be issued on the command line on either 32-bit or 64-bit Windows. It will pull the location of notepad++.exe from the registry and configure git to use it automatically:


```
#!cmd

FOR /F "usebackq tokens=2*" %A IN (`REG QUERY "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\App Paths\notepad++.exe" /ve`) DO git config --global core.editor "'%B' -multiInst -notabbar -nosession -noPlugin"
```

If you wish to place the above from a .BAT or .CMD file, you must replace %A with %%A and %B with %%B

## Исправление fatal: LF would be replaced by CRLF in ##
Данную проблему довольно просто решить текстовым редактором Notepad++ (в котором кстати некоторые гуру программирования умудряются писать проекты). Для исправления переносов строк нужно сделать следующее:

1. Открываем указанный IDE файл в Notepad++.
2. В меню выбираем Правка->Формат конца строк->Преобразовать в Win-формат.
3. Сохраняем файл в Notepad++.
4. Пробуем еще раз добавить файл в Git.

И хорошо, если это был один файл, но что делать, если файлов много, и они вперемешку, unix и win? Самое простое, что можно сделать — отключить проверку формата в настройках git, установленного в систему:


```
#!cmd

> git config --global core.autocrlf false
> git config --global core.safecrlf false
```

Но это не всегда помогает.